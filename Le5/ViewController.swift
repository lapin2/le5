//
//  ViewController.swift
//  Le5
//
//  Created by student on 09.09.18.
//  Copyright © 2018 lapin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        roundTarget(9)

    }

    func drawBox(_ boxQuantity: Int) {
        let boxSide = 32
        var boxArray = [UIView]()
        var rectArray = [CGRect]()
        for n in 0..<boxQuantity {
            rectArray.append(CGRect.init(x: 20 + n*boxSide + n*10, y: 40, width: boxSide, height: boxSide))
            boxArray.append(UIView.init(frame: rectArray[n]))
            boxArray[n].backgroundColor = .green
            view.addSubview(boxArray[n])
        }
    }

    func drawStairs(_ boxQuantity: Int) {
        let boxSide = 32
        var boxArray = [UIView]()
        var rectArray = [CGRect]()
        var boxesInRow = 1
        for m in 0..<boxQuantity {
            for n in 0..<boxesInRow {
                rectArray.append(CGRect.init(x: 20 + n*boxSide + n*10, y: 40 + m*boxSide + m*10, width: boxSide, height: boxSide))
            }
            boxesInRow += 1
        }
        for l in 0..<rectArray.count {
            boxArray.append(UIView.init(frame: rectArray[l]))
            boxArray[l].backgroundColor = .blue
            view.addSubview(boxArray[l])
        }
    }

    func drawPyramid (_ boxQuantity: Int) {
        let boxSide = 32
        var boxArray = [UIView]()
        var rectArray = [CGRect]()
        var boxesInRow = 1
        var a = 5*(boxQuantity - 1)
        for m in 0..<boxQuantity {
            for n in 0..<boxesInRow {
                rectArray.append(CGRect.init(x: 20 + a + (boxQuantity*boxSide/2) - (boxSide/2*boxesInRow) + n*boxSide + n*10, y: 40 + m*boxSide + m*10, width: boxSide, height: boxSide))
            }
            boxesInRow += 1
            a -= 5
        }
        for l in 0..<rectArray.count {
            boxArray.append(UIView.init(frame: rectArray[l]))
            boxArray[l].backgroundColor = .red
            view.addSubview(boxArray[l])
        }
    }

    func target (_ lineQuantity: Int) {
        var targetSide = 5 + 20*lineQuantity
        var targetArray = [UIView]()
        var rectArray = [CGRect]()
        for n in 0..<lineQuantity {
            rectArray.append(CGRect.init(x: 20 + n*10, y: 40 + n*10, width: targetSide, height: targetSide))
            targetArray.append(UIView.init(frame: rectArray[n]))
            targetSide -= 20
            if n % 2 > 0 {
                targetArray[n].backgroundColor = .yellow
            }
            else {
                targetArray[n].backgroundColor = .blue
            }
            view.addSubview(targetArray[n])
        }
    }

    func roundTarget (_ lineQuantity: Int) {
        var targetSide = 5 + 20*lineQuantity
        var targetArray = [UIView]()
        var rectArray = [CGRect]()
        for n in 0..<lineQuantity {
            rectArray.append(CGRect.init(x: 20 + n*10, y: 40 + n*10, width: targetSide, height: targetSide))
            targetArray.append(UIView.init(frame: rectArray[n]))
            targetSide -= 20
            if n % 2 > 0 {
                targetArray[n].backgroundColor = .yellow
            }
            else {
                targetArray[n].backgroundColor = .blue
            }
            targetArray[n].layer.cornerRadius = rectArray[n].width / 2
            view.addSubview(targetArray[n])
        }

    }



}

